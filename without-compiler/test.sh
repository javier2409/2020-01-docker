#!/bin/sh

if docker run --rm $(docker build -q .) ; then
	echo "OK"
else
	echo "Failed"
	exit 1
fi
